<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<c:if test="${doRedirect}">
        <meta http-equiv="refresh" content="3;url=${redirectTo }" />
    </c:if>
    <!-- bower:css -->
    <link rel="stylesheet" href="assets/public/lib/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/public/lib/datatables/media/css/jquery.dataTables.css" />
    <link rel="stylesheet" href="assets/public/lib/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="assets/public/lib/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="assets/public/lib/morrisjs/morris.css" />
    <link rel="stylesheet" href="assets/public/lib/datatables-responsive/css/dataTables.responsive.css" />
    <link rel="stylesheet" href="assets/public/lib/bootstrap-social/bootstrap-social.css" />
    <link rel="stylesheet" href="assets/public/lib/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css" />
    <!-- endbower -->
    <link rel="stylesheet" href="assets/public/lib/notifications/css/ns-default.css" />
    <link rel="stylesheet" href="assets/public/lib/notifications/css/ns-style-bar.css" />

    <!-- bower:js -->
    <script src="assets/public/lib/jquery/dist/jquery.js"></script>
    <script src="assets/public/lib/bootstrap/dist/js/bootstrap.js"></script>
    <script src="assets/public/lib/datatables/media/js/jquery.dataTables.js"></script>
    <script src="assets/public/lib/flot/jquery.flot.js"></script>
    <script src="assets/public/lib/holderjs/holder.js"></script>
    <script src="assets/public/lib/metisMenu/dist/metisMenu.js"></script>
    <script src="assets/public/lib/eve/eve.js"></script>
    <script src="assets/public/lib/raphael/raphael.min.js"></script>
    <script src="assets/public/lib/mocha/mocha.js"></script>
    <script src="assets/public/lib/morrisjs/morris.js"></script>
    <script src="assets/public/lib/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="assets/public/lib/flot.tooltip/js/jquery.flot.tooltip.js"></script>
    <script src="assets/public/lib/bootstrap-validator/dist/validator.js"></script>
    <script src="assets/public/lib/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js"></script>
    <script src="assets/public/lib/sockjs-client/dist/sockjs.js"></script>
    <script src="assets/public/lib/stomp-websocket/lib/stomp.min.js"></script>
    <!-- endbower -->
    
    <script src="assets/public/lib/notifications/js/classie.js"></script>
    <script src="assets/public/lib/notifications/js/modernizr.custom.js"></script>
    <script src="assets/public/lib/notifications/js/notificationFx.js"></script>

    <!-- inject:css -->
    <link rel="stylesheet" href="assets/public/css/global.css"></link>
    <link rel="stylesheet" href="assets/public/css/home-page.css"></link>
    <!-- endinject -->

    <!-- inject:js -->
    <script src="assets/public/js/main-wo.js"></script>
    <script src="assets/public/js/sockjs-0.3.4.min.js"></script>
    <!-- endinject -->
    
	<title>Website Observer Home</title>
</head>
<body>
    
<!-- MODAL -->
    <div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal">
    					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    				</button>
    				<h3 class="modal-title" id="modal-register-label">Sign up now</h3>
    				<p>Fill in the form below to get instant access:</p>
    			</div>
    			
    			<div class="modal-body">
    				
                 <form:form commandName="user" role="form" action="/WebsiteObserver/signUp" method="post" class="registration-form">
                 	<div class="form-group">
                 		<label>First name</label>
                     	<form:input path="firstName" type="text" class="form-first-name form-control"/>
                     </div>
                     <div class="form-group">
                     	<label>Last name</label>
                     	<form:input path="lastName" type="text" class="form-last-name form-control" />
                     </div>
                     <div class="form-group">
                     	<label>Email</label>
                     	<form:input path="email" type="text"  class="form-email form-control" />
                     </div>
                     <div class="form-group">
                     	<label>Password</label>
                     	<form:input path="password" type="password" class="form-email form-control" />
                     </div>
                     <div class="form-group">
                     	<label>Retype Password</label>
                     	<input type="password" class="form-email form-control" />
                     </div>
                     <button type="submit" class="btn">Sign me up!</button>
                 </form:form>
                 
    			</div>
    			
    		</div>
    	</div>
    </div>
    
<!-- MODAL -->
    <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal">
    					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    				</button>
    				<h3 class="modal-title" id="modal-login-label">Sign up now</h3>
    				<p>Fill in the form below to get instant access:</p>
    			</div>
    			
    			<div class="modal-body">
                 <form:form commandName="user" role="form" action="/WebsiteObserver/signIn" method="post" class="login-form">
                     <div class="form-group">
                     	<label>Email</label>
                     	<form:input path="email" type="text"  class="form-email form-control" />
                     </div>
                     <div class="form-group">
                     	<label>Password</label>
                     	<form:input path="password" type="password" class="form-email form-control" />
                     </div>
                     <button type="submit" class="btn">Sign in!</button>
                 </form:form>
                 
    			</div>
    			
    		</div>
    	</div>
    </div>
    
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#">
					<img class="logotype" src="assets/img/logo.png"/>
					<span class="color-blue">Website</span><span class="color-orange">Observer</span>
				</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" class="launch-modal" data-modal-id="modal-login">
                        	<i class="fa fa-sign-in"></i> Login
                        </a>
                    </li>
                    <li>
                        <a href="#" class="launch-modal" data-modal-id="modal-register">
                        	<i class="fa fa-pencil-square-o"></i> Register
                        </a>
                    </li>
                    <li>
                        <a href="#contact" class="help">
                        	<i class="fa fa-question-circle"></i> Help
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	

    <!-- Header -->
    <div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1><span class="color-blue">Website</span><span class="color-orange">Observer</span></h1>
                        <h3 style="color:white">Observe all site changes, receive notification</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <li>
                                <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">           	
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#about">About</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#services">Services</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#contact">Contact</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; "andrushko-software" - 2016. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript">
         $(document).ready(function() {
            var flag = '${notification.isAvailable()}';
            if(flag === 'true') {
            	var notification = new NotificationFx({
	       			message : '<i class="${notification.getIcon()}"></i><p>${notification.getMessage()}</p>',
	       			layout : 'bar',
	       			effect : 'slidetop',
	       			type : 'notice',
	       			bg: '${notification.getBgType()}'
         		});
            	notification.show();
            }
		});
     </script>
</body>
</html>