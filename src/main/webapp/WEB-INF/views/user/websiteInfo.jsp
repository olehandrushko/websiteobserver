<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- bower:css -->
	<link rel="stylesheet" href="../assets/public/lib/bootstrap/dist/css/bootstrap.css" />
	<link rel="stylesheet" href="../assets/public/lib/datatables/media/css/jquery.dataTables.css" />
	<link rel="stylesheet" href="../assets/public/lib/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="../assets/public/lib/metisMenu/dist/metisMenu.css" />
	<link rel="stylesheet" href="../assets/public/lib/morrisjs/morris.css" />
	<link rel="stylesheet" href="../assets/public/lib/datatables-responsive/css/dataTables.responsive.css" />
	<link rel="stylesheet" href="../assets/public/lib/bootstrap-social/bootstrap-social.css" />
	<link rel="stylesheet" href="../assets/public/lib/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css" />
	<!-- endbower -->
	
	<!-- inject:css -->
	<link rel="stylesheet" href="../assets/public/css/global.css"></link>
	<link rel="stylesheet" href="../assets/public/css/home-page.css"></link>
	<link rel="stylesheet" href="../assets/public/css/user/home-user.css"></link>
	<!-- endinject -->
	
</head>
<body>
<div id="wrapper">

        <jsp:include page="parts/navbars.jsp"></jsp:include>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                	<h3>Your websites: ${currentUser.getEmail()}</h3>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Your observing websites: 
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-tgl dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="/WebsiteObserver/websiteAdd">Add website</a>
                                        </li>
                                        <li><a href="#">Another action</a>
                                        </li>
                                        <li><a href="#">Something else here</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- <div id="morris-area-chart"></div> -->
                            <table class="table table-striped table-hover ">
							  <thead>
							    <tr>
							      <th>№</th>
							      <th>Website name</th>
							      <th>Visit</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<c:forEach items="${website.getWebPages()}" var="webpage">
								  	<tr>
								      <td>${webpage.getId() }</td>
								      <td>${webpage.getUrl() }</td>
								    </tr>
							    	<c:out value="${i}"/><p>
								</c:forEach>						  
							  </tbody>
						  	</table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

	<jsp:include page="parts/footer.jsp"></jsp:include>
	
<!-- bower:js -->
<script src="../assets/public/lib/jquery/dist/jquery.js"></script>
<script src="../assets/public/lib/bootstrap/dist/js/bootstrap.js"></script>
<script src="../assets/public/lib/datatables/media/js/jquery.dataTables.js"></script>
<script src="../assets/public/lib/flot/jquery.flot.js"></script>
<script src="../assets/public/lib/holderjs/holder.js"></script>
<script src="../assets/public/lib/metisMenu/dist/metisMenu.js"></script>
<script src="../assets/public/lib/eve/eve.js"></script>
<script src="../assets/public/lib/raphael/raphael.min.js"></script>
<script src="../assets/public/lib/mocha/mocha.js"></script>
<script src="../assets/public/lib/morrisjs/morris.js"></script>
<script src="../assets/public/lib/datatables-responsive/js/dataTables.responsive.js"></script>
<script src="../assets/public/lib/flot.tooltip/js/jquery.flot.tooltip.js"></script>
<script src="../assets/public/lib/bootstrap-validator/dist/validator.js"></script>
<script src="../assets/public/lib/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js"></script>
<script src="../assets/public/lib/sockjs-client/dist/sockjs.js"></script>
<script src="../assets/public/lib/stomp-websocket/lib/stomp.min.js"></script>
<!-- endbower -->

<!-- inject:js -->
<script src="../assets/public/js/main-wo.js"></script>
<script src="../assets/public/js/sockjs-0.3.4.min.js"></script>
<!-- endinject -->
</body>
</html>