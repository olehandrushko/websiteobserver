<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- bower:css -->
<link rel="stylesheet" href="../assets/public/lib/bootstrap/dist/css/bootstrap.css" />
<link rel="stylesheet" href="../assets/public/lib/datatables/media/css/jquery.dataTables.css" />
<link rel="stylesheet" href="../assets/public/lib/font-awesome/css/font-awesome.css" />
<link rel="stylesheet" href="../assets/public/lib/metisMenu/dist/metisMenu.css" />
<link rel="stylesheet" href="../assets/public/lib/morrisjs/morris.css" />
<link rel="stylesheet" href="../assets/public/lib/datatables-responsive/css/dataTables.responsive.css" />
<link rel="stylesheet" href="../assets/public/lib/bootstrap-social/bootstrap-social.css" />
<link rel="stylesheet" href="../assets/public/lib/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css" />
<!-- endbower -->

<!-- inject:css -->
<link rel="stylesheet" href="../assets/public/css/global.css"></link>
<link rel="stylesheet" href="../assets/public/css/home-page.css"></link>
<link rel="stylesheet" href="../assets/public/css/user/home-user.css"></link>
<!-- endinject -->

</head>
<body>
	<div id="wrapper">
		<jsp:include page="parts/navbars.jsp"></jsp:include>



		<div id="page-wrapper">
		
			

			<div class="row">
				<h1>Messaging Form</h1>
				<br>
		
				<div id="feedback"></div>
		
				<form class="form-horizontal" id="search-form">
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Code</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="codeInput">
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Message</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="messageInput">
						</div>
					</div>
		
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="bth-search"
								class="btn btn-primary btn-lg">Submit</button>
						</div>
					</div>
				</form>
		
			</div>

			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">User settings</h1>
					<form class="form-horizontal">
						<fieldset>
							<legend>Legend</legend>
							<div class="form-group">
								<label for="inputEmail" class="col-lg-2 control-label">Email</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="inputEmail"
										placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword" class="col-lg-2 control-label">Password</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" id="inputPassword"
										placeholder="Password">
									<div class="checkbox">
										<label> <input type="checkbox"> Checkbox
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="textArea" class="col-lg-2 control-label">Textarea</label>
								<div class="col-lg-10">
									<textarea class="form-control" rows="3" id="textArea"></textarea>
									<span class="help-block">A longer block of help text
										that breaks onto a new line and may extend beyond one line.</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Radios</label>
								<div class="col-lg-10">
									<div class="radio">
										<label> <input type="radio" name="optionsRadios"
											id="optionsRadios1" value="option1" checked="">
											Option one is this
										</label>
									</div>
									<div class="radio">
										<label> <input type="radio" name="optionsRadios"
											id="optionsRadios2" value="option2"> Option two can
											be something else
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="select" class="col-lg-2 control-label">Selects</label>
								<div class="col-lg-10">
									<select class="form-control" id="select">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select> <br> <select multiple="" class="form-control">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="reset" class="btn btn-default">Cancel</button>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>


	</div>


	<!-- bower:js -->
	<script src="../assets/public/lib/jquery/dist/jquery.js"></script>
	<script src="../assets/public/lib/bootstrap/dist/js/bootstrap.js"></script>
	<script src="../assets/public/lib/datatables/media/js/jquery.dataTables.js"></script>
	<script src="../assets/public/lib/flot/jquery.flot.js"></script>
	<script src="../assets/public/lib/holderjs/holder.js"></script>
	<script src="../assets/public/lib/metisMenu/dist/metisMenu.js"></script>
	<script src="../assets/public/lib/eve/eve.js"></script>
	<script src="../assets/public/lib/raphael/raphael.min.js"></script>
	<script src="../assets/public/lib/mocha/mocha.js"></script>
	<script src="../assets/public/lib/morrisjs/morris.js"></script>
	<script src="../assets/public/lib/datatables-responsive/js/dataTables.responsive.js"></script>
	<script src="../assets/public/lib/flot.tooltip/js/jquery.flot.tooltip.js"></script>
	<script src="../assets/public/lib/bootstrap-validator/dist/validator.js"></script>
	<script src="../assets/public/lib/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js"></script>
	<script src="../assets/public/lib/sockjs-client/dist/sockjs.js"></script>
	<script src="../assets/public/lib/stomp-websocket/lib/stomp.min.js"></script>
	<!-- endbower -->

	<!-- inject:js -->
	<script src="../assets/public/js/main-wo.js"></script>
	<script src="../assets/public/js/sockjs-0.3.4.min.js"></script>
	<!-- endinject -->



<script>
	jQuery(document).ready(function($) {

		$("#search-form").submit(function(event) {
			enableSearchButton(false);
			event.preventDefault();
			searchViaAjax();
		});

	});

	function searchViaAjax() {

		var requestBody = {}
		requestBody["requestMessage"] = $("#messageInput").val();
		requestBody["code"] = $("#codeInput").val();

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "/WebsiteObserver/api/testRequest",
			data : JSON.stringify(requestBody),
			dataType : 'json',
			timeout : 100000,
			success : function(data) {
				console.log("SUCCESS: ", data);
				display(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
				display(e);
			},
			done : function(e) {
				console.log("DONE");
				enableSearchButton(true);
			}
		});

	}

	function enableSearchButton(flag) {
		$("#btn-search").prop("disabled", flag);
	}

	function display(data) {
		var json = "<h4>Ajax Response</h4><pre>"
				+ JSON.stringify(data, null, 4) + "</pre>";
		$('#feedback').html(json);
	}
</script>


</body>
</html>