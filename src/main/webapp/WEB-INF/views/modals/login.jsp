<!-- MODAL -->
    <div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal">
    					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    				</button>
    				<h3 class="modal-title" id="modal-login-label">Sign up now</h3>
    				<p>Fill in the form below to get instant access:</p>
    			</div>
    			
    			<div class="modal-body">
                 <form role="form" action="" method="post" class="registration-form">
                 	<div class="form-group">
                 		<label class="sr-only" for="form-first-name">First name</label>
                     	<input type="text" name="form-first-name" placeholder="First name..." class="form-first-name form-control" id="form-first-name">
                     </div>

                     <div class="form-group">
                     	<label class="sr-only" for="form-email">Email</label>
                     	<input type="text" name="form-email" placeholder="Email..." class="form-email form-control" id="form-email">
                     </div>

                     <button type="submit" class="btn">Sign in!</button>
                 </form>
                 
    			</div>
    			
    		</div>
    	</div>
    </div>