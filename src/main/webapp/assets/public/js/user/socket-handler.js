jQuery(document).ready(function($) {	
	
	var selectors = {
		commandInput: "#txtSendMessage",
		commandSendButton: "#sendMessage",
		connect: "#connect",
		disconnect: "#disconnect",
		responseBox: "#response"
	};
	
	$("[data-website-id]").click(function(event){
		var self = this;
		
		startSiteObserveClick();

        var requestBody = {
			websiteId: $(self).data("website-id"),
			requestMessage: "Start observing",
			code: 1
		}
        
		connect(function() {
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/WebsiteObserver/api/v1/startObserving",
				data : JSON.stringify(requestBody),
				//dataType : 'json',
				timeout : 100000,
				success : function(data) {
					console.log("SUCCESS: ", data);
					displayConsole(data);
				},
				error : function(e) {
					console.log("ERROR: ", e);
					displayConsole(e);
				}
			}).done(function(data) {
				displayConsole(data);
				endSiteObserveClick();
			});
		});

		function startSiteObserveClick(){
			
			setDisabled(self, selectors.disconnect, selectors.commandInput, selectors.commandSendButton, true);
		}
		function endSiteObserveClick(){
			setDisabled(self, false);
		}
	});
	

	
	//WEBSOCKET globals
    var stompClient = null;
	var isConnected = false;
	
	function connect(callback) {
        $(selectors.responseBox).empty();
        cmdTextboxFocuse();

        var socket = new SockJS('/WebsiteObserver/simplemessages');
        stompClient = Stomp.over(socket);
        
        stompClient.connect('', '', function(frame) {         
            setConnected(true);
            console.log("Connected: " + frame);
            showMessage("Connection established: " + frame, false);

            stompClient.subscribe("/topic/analyzeWebsite", function(data) {
        		showMessage(data, false);
            });
            
            if(callback){
            	callback();
            }
        }, function(message) {
            connect(callback);
        });
    }

	function setConnected(connected) {
    	isConnected = connected;
        $(selectors.connect).prop('disabled', connected);
        $(selectors.disconnect).prop('disabled', !connected);
        $(selectors.commandSendButton).prop('disabled', !connected);
        $(selectors.commandInput).prop('disabled', !connected)
    }
	
	
	function displayConsole(data) {
		var json = JSON.stringify(data, null, 4);
		console.log(json, false);
	}
	
	/* messageText,messageColor, messageIcon, messageIconColor */
    function showMessage(data) {
    	var messageObject = {
			messageText: data,
			messageColor: "#d8bfd8",
			messageIcon: "fa fa-arrow-right",
			messageIconColor: "#d8bfd8"
    	};
    	
    	if(data.body && isJson(data.body)){
    		messageObject = JSON.parse(data.body);
    	}
    	
		$(selectors.responseBox).append('<p>' 
			+ '<i class="' + messageObject.messageIcon + '" style="color:' + messageObject.messageIconColor + '" ></i>'
			+ '<span style="color:' + messageObject.messageIconColor + '" >[' +getCurrentDateTime()+"] "+ messageObject.messageText + '</span></p>');
		
		$(selectors.responseBox).animate({ scrollTop: $(selectors.responseBox).prop("scrollHeight")}, 200);
    }
	
    function disconnect() {
        stompClient.disconnect();
        setConnected(false);
        
        console.log("Disconnected");
        showMessage("WebSocket connection is now terminated!", true);
    }

    function sendMessageToServer(messageForServer) {
    	var data = eval('('+'${currentUser}'+')'); 
        showMessage("Your message '" + messageForServer + "' is being sent to the server.", true);
        stompClient.send("/app/simplemessages", {}, JSON.stringify({
            'message' : messageForServer
        }));
    }
	
	
    $(selectors.connect).on("click", function(e) {
        connect();	
    });

    $(selectors.disconnect).on("click", function(e) {
        disconnect();
        
        var requestBody = {
    			websiteId: 100,
    			requestMessage: "Start observing",
    			code: 1
    		}
        $.ajax({
			type : "POST",
			contentType : "application/json",
			url : "/WebsiteObserver/api/v1/stopObserving",
			data : JSON.stringify(requestBody),
			//dataType : 'json',
			timeout : 100000,
			success : function(data) {
				console.log("SUCCESS: ", data);
				displayConsole(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
				displayConsole(e);
			}
		}).done(function(data) {
			displayConsole(data);
			endSiteObserveClick();
		})
    });
    
    $(selectors.commandSendButton).on("click", function(e) {
        var messageForServer = $(selectors.commandInput).val();
        if (messageForServer === "") {
            e.preventDefault();
        } else {
            sendMessageToServer(messageForServer);
        }
    });
	
    function cmdTextboxFocuse(){
    	$(selectors.commandInput).val("");
        $(selectors.commandInput).focus();
        $(selectors.commandInput).select();    	
    }
    
	function isJson(str) {
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}
	
    function getCurrentDateTime() {
        var date = new Date();
        var n = date.toDateString();
        var time = date.toLocaleTimeString();
        return n + " @ " + time;
    }
    
    function setDisabled(data, value){
    	if(Array.isArray(data)){
    		data.each(function( i ) {
    			$(this).attr("disabled", value);
    		});
    	} else if(typeof data === 'string'){
    		$(selector).attr("disabled", value);
    	}
    }
});