var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	jscs = require('gulp-jscs'),
	nodemon = require('gulp-nodemon');

var jsFiles = ['./public/js/*.js'];

gulp.task('style', function(){
	return gulp.src(jsFiles).pipe(jshint())
							.pipe(jscs())
							.pipe(jshint.reporter('jshint-stylish', 
								{
									verbose: true
								}));
})

var replacer;
var publicReplacer = 'assets/public';
var userReplacer = '../assets/public';


var wiredep = require('wiredep'),
	inject = require('gulp-inject');

var gulpInjectJSSrc = gulp.src(['./public/js/*.js'], {read: false}),
	gulpInjectJSOptions = {
		ignorePath: '',
		transform : function ( filePath, file, i, length ) {
            var newPath = filePath.replace( '/public', publicReplacer );
            console.log('inject script = '+ newPath);
            return '<script src="' + newPath  + '"></script>';
        }
	};

var gulpInjectCSSSrc = gulp.src(['./public/css/*.css'], {read: false}),
	gulpInjectCSSOptions = {
		ignorePath: '',
		transform : function ( filePath, file, i, length ) {
            var newPath = filePath.replace( '/public', publicReplacer );
            console.log('inject style = '+ newPath);
            return '<link rel="stylesheet" href="' + newPath  + '"></link>';
        }
	};

var gulpInjectJSSrcAdmin = gulp.src(['./public/js/*.js', './public/js/user/*.js'], {read: false}),
gulpInjectJSOptionsAdmin = {
	ignorePath: '',
	transform : function ( filePath, file, i, length ) {
        var newPath = filePath.replace( '/public', userReplacer );
        console.log('inject script = '+ newPath);
        return '<script src="' + newPath  + '"></script>';
    }
};

var gulpInjectCSSSrcAdmin = gulp.src(['./public/css/*.css', './public/css/user/*.css'], {read: false}),
gulpInjectCSSOptionsAdmin = {
	ignorePath: '',
	transform : function ( filePath, file, i, length ) {
        var newPath = filePath.replace( '/public', userReplacer );
        console.log('inject style = '+ newPath);
        return '<link rel="stylesheet" href="' + newPath  + '"></link>';
    }
};


var	options = {
		bowerJson: require('./bower.json'),
		directory: './public/lib',
		ignorePath: '../../',
		fileTypes: {
		    jsp: {
		      block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
		      detect: {
		        js: /<script.*src=['"]([^'"]+)/gi,
		        css: /<link.*href=['"]([^'"]+)/gi
		      },
		      replace: {
		        js: '<script src="{{filePath}}"></script>',
		        css: '<link rel="stylesheet" href="{{filePath}}" />'
		      }
		    }
		}
	};	



gulp.task('inject-public', function(){
	replacer = publicReplacer;
	return gulp.src('../WEB-INF/views/*.jsp')
						 .pipe(wiredep.stream(options))
						 .pipe(inject(gulpInjectJSSrc, gulpInjectJSOptions))
						 .pipe(inject(gulpInjectCSSSrc, gulpInjectCSSOptions))
						 .pipe(gulp.dest('../WEB-INF/views/'));
});

gulp.task('inject-user', function(){
	replacer = userReplacer;
	return gulp.src('../WEB-INF/views/user/*.jsp')
						 .pipe(wiredep.stream(options))
						 .pipe(inject(gulpInjectJSSrcAdmin, gulpInjectJSOptionsAdmin))
						 .pipe(inject(gulpInjectCSSSrcAdmin, gulpInjectCSSOptionsAdmin))
						 .pipe(gulp.dest('../WEB-INF/views/user/'));
});

gulp.task('inject', ['inject-public', 'inject-user']);

gulp.task('serve', ['style', 'inject'], function(){
	var options = {
		script: 'app.js',
		delayTime: 1,
		env: {
			'PORT': 4000
		},
		watch: jsFiles
	};
	return nodemon(options).on('restart', function(){
		console.log('Restarting server...');
	});
});