SELECT * FROM websiteobserver.webpage;

SELECT * FROM websiteobserver.user;

SELECT * FROM websiteobserver.website;

SELECT * FROM websiteobserver.website_user;

SELECT * FROM websiteobserver.website_webpage;

SET SQL_SAFE_UPDATES = 0;
SET FOREIGN_KEY_CHECKS=0; 
/*DELETE FROM websiteobserver.user;
DELETE FROM websiteobserver.website;*/
DELETE FROM websiteobserver.webpage;
SET SQL_SAFE_UPDATES = 1;
SET FOREIGN_KEY_CHECKS=1;