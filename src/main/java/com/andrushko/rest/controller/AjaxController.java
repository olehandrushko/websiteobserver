package com.andrushko.rest.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.andrushko.rest.api.AjaxRequestBody;
import com.andrushko.rest.api.AjaxResponseBody;
import com.andrushko.rest.helpers.Views;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class AjaxController
{

	// @ResponseBody, not necessary, since class is annotated with
	// @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped
	// by field name.
	// @JsonView(Views.Public.class) - Optional, filters json data to display.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/api/testRequest", method = RequestMethod.POST)
	public AjaxResponseBody getSearchResultViaAjax(
			@RequestBody AjaxRequestBody requestBody)
	{
		AjaxResponseBody result = new AjaxResponseBody();
		result.setMessage("Hi, client! I have received your message : '"
				+ requestBody.getRequestMessage() 
				+ "[" + requestBody.getCode() + "]");

		return result;

	}

}
