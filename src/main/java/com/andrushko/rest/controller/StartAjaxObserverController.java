package com.andrushko.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.andrushko.annotation.Authenticate;
import com.andrushko.entity.User;
import com.andrushko.entity.Website;
import com.andrushko.rest.api.AjaxResponseBody;
import com.andrushko.rest.api.AjaxStartRobot;
import com.andrushko.rest.helpers.Views;
import com.andrushko.service.IMessangeService;
import com.andrushko.service.INewService;
import com.andrushko.service.IWebpageService;
import com.andrushko.service.IWebsiteService;
import com.andrushko.utils.Constants;
import com.andrushko.webrobot.RunnableRobot;
import com.andrushko.webrobot.WebRobot;
import com.fasterxml.jackson.annotation.JsonView;


@Authenticate
@RestController
@SessionAttributes("currentUser")
public class StartAjaxObserverController
{
	@Autowired
	private IMessangeService messageService;
	@Autowired
	private INewService newService;
	@Autowired
	private IWebsiteService websiteService;
	@Autowired
	private IWebpageService webpageService;
	@Autowired
	private MessageSendingOperations<String> messagingTemplate;
	private Thread robotThread;
	
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/api/v1/startObserving", method = RequestMethod.POST)
	public AjaxResponseBody startObserving(
			@RequestBody AjaxStartRobot requestBody, 
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser)
	{
		AjaxResponseBody result = new AjaxResponseBody();
		result.setMessage("Hi, client! I have received your message : '"
				+ requestBody.getRequestMessage() 
				+ "[" + requestBody.getCode() + "]");

		Website website = websiteService.getUserWebsite(currentUser, requestBody.getWebsiteId());
		WebRobot webRobot = new WebRobot(website, currentUser, webpageService,
				websiteService, messagingTemplate, messageService, newService);
		robotThread = new Thread(new RunnableRobot(webRobot, 5000));

		robotThread.start();
		
		return result;
	}
	
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/api/v1/stopObserving", method = RequestMethod.POST)
	public AjaxResponseBody stopObserving(
			@RequestBody AjaxStartRobot requestBody, 
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser)
	{
		AjaxResponseBody result = new AjaxResponseBody();
		result.setMessage("Hi, client! I have received your message : '"
				+ requestBody.getRequestMessage() 
				+ "[" + requestBody.getCode() + "]");


		robotThread.stop();
		return result;
	}
	
}
