package com.andrushko.rest.helpers;

public class RestCode
{
	public static class Request
	{
		public static final int START_OBSERVING = 1;
		public static final int SIMPLE_MESSAGE = 2;
	}
	
	public static class RESPONSE
	{
		public static final int OK = 1;
		public static final int ERROR = 2;
	} 
}
