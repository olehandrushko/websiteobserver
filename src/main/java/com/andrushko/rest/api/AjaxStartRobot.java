package com.andrushko.rest.api;

public class AjaxStartRobot extends AjaxRequestBody
{
	private int websiteId;

	public int getWebsiteId()
	{
		return websiteId;
	}

	public void setWebsiteId(int websiteId)
	{
		this.websiteId = websiteId;
	}
	
}
