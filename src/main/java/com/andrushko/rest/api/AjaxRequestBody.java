package com.andrushko.rest.api;

public class AjaxRequestBody
{
	private String requestMessage;
	private int code;
	
	public String getRequestMessage()
	{
		return requestMessage;
	}
	public void setRequestMessage(String requestMessage)
	{
		this.requestMessage = requestMessage;
	}
	public int getCode()
	{
		return code;
	}
	public void setCode(int code)
	{
		this.code = code;
	}	
}
