package com.andrushko.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.andrushko.entity.User;
import com.andrushko.model.Notification;
import com.andrushko.service.IUserService;
import com.andrushko.utils.Constants;
import com.andrushko.utils.MailUtils;
import com.andrushko.utils.StringUtils;

@Controller
public class HomeController
{
	@Autowired
	private IUserService userService;

	private MailUtils mailUtil;

	public HomeController()
	{
		super();
		mailUtil = new MailUtils();
	}

	// INDEX
	@RequestMapping(value = "/")
	public String index(Model model, HttpSession session) throws Exception
	{
		if ((User) session.getAttribute("user") != null)
		{
			return "redirect:/user/home";
		}

		model.addAttribute("user", new User());
		return "index";
	}

	// SIGN-UP
	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public String signUp(@ModelAttribute("user") User user, Model model,
			HttpSession session, HttpServletRequest request)
	{
		if (userService.getUser(user.getEmail()) == null)
		{
			userService.registerUser(user);
			if (user.getAuthenticationCode() != null)
			{
				String mailMessageBody = StringUtils.concat(
						"Thanks for registration, click on link below to activate your accaunt:\n",
						StringUtils.getURLWithContextPath(request), "/activate",
						"?email=", user.getEmail(), "&authCode=",
						user.getAuthenticationCode());

				mailUtil.sendMessage(user.getEmail(), mailMessageBody);

				Notification.build(
						"Thanks for registration! Check your email to activate your account.",
						"fa fa-envelope", Constants.NotificationType.SUCCESS);
				model.addAttribute("notification", Notification.getInstance());
			}
		}
		return "index";
	}

	// SIGN-IN
	@RequestMapping(value = "/signIn", method = RequestMethod.POST)
	public String signIn(@ModelAttribute("user") User user, Model model,
			HttpSession session)
	{
		String enteredEmail = user.getEmail();
		String enteredPassword = user.getPassword();

		User dbUser = userService.getUser(enteredEmail);
		if (dbUser == null)
		{
			Notification.build(
					"Such email address is not registered in the system.",
					"fa fa-exclamation-triangle", 3);
			model.addAttribute("notification", Notification.getInstance());
			return "index";
			
		} else if (userService.isValidCreadentials(enteredEmail,
				enteredPassword) && dbUser.isAccauntActivated())
		{
			session.setAttribute(Constants.MainConstants.CURRENTUSERSESSIONID,
					userService.getUser(enteredEmail));
			return "redirect:/user/home";
		} else {
			Notification.build(
					"Your password is wrong! Please, try to retype it!",
					"fa fa-exclamation-triangle", 3);
			model.addAttribute("notification", Notification.getInstance());
			return "index";
		}
	}

	// ACTIVATE ACCAUNT
	@RequestMapping(value = "/activate", method = RequestMethod.GET, params = {
			"email", "authCode" })
	public String activate(@RequestParam("email") String email,
			@RequestParam("authCode") String authCode, Model model,
			HttpSession session, HttpServletRequest request)
	{
		User dbUser = userService.getUser(email);
		if (dbUser != null && dbUser.getAuthenticationCode().equals(authCode)
				&& !dbUser.isAccauntActivated())
		{
			dbUser.setAccauntActivated(true);
			userService.updateUser(dbUser);
		}
		
		Notification.build(
				"Your account is activating... Wait a minute!",
				"fa fa-clock-o ", 2);
		model.addAttribute("notification", Notification.getInstance());
		model.addAttribute("user", dbUser);
		model.addAttribute("doRedirect", true);
		model.addAttribute("redirectTo", StringUtils.getURLWithContextPath(request));
		
		return "index";
	}

	// SIGN-OUT
	@RequestMapping(value = "/signOut", method = RequestMethod.GET)
	public String signOut(Model model, HttpSession session)
	{
		if ((User) session.getAttribute("user") != null)
		{
			session.removeAttribute("user");
		}
		return "redirect:/";

	}
	
}
