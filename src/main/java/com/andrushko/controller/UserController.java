package com.andrushko.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.andrushko.annotation.Authenticate;
import com.andrushko.entity.User;
import com.andrushko.utils.Constants;
import com.andrushko.viewmodel.LayoutViewModel;

@Authenticate
@Controller
@SessionAttributes({"currentUser","layoutVm"})
public class UserController
{	
	// USER-HOME
	@RequestMapping(value = "/user/home")
	public String index(
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser,
			@ModelAttribute(Constants.MainConstants.LAYOUTVM) LayoutViewModel layoutVm,
			Model model, HttpSession session)
	{		
		return "user/home";
	}

	@RequestMapping(value = "/user/settings")
	public String settings(
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser,
			Model model, HttpSession session)
	{
		model.addAttribute(Constants.MainConstants.CURRENTUSERSESSIONID, currentUser);

		return "user/settings";
	}

}
