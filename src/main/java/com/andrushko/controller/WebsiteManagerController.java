package com.andrushko.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.andrushko.annotation.Authenticate;
import com.andrushko.entity.User;
import com.andrushko.entity.Webpage;
import com.andrushko.entity.Website;
import com.andrushko.service.IMessangeService;
import com.andrushko.service.INewService;
import com.andrushko.service.IWebpageService;
import com.andrushko.service.IWebsiteService;
import com.andrushko.utils.Constants;
import com.andrushko.webrobot.RunnableRobot;
import com.andrushko.webrobot.WebRobot;

@Authenticate
@Controller
@SessionAttributes({"currentUser", "layoutVm"})
public class WebsiteManagerController
{
	@Autowired
	private IMessangeService messageService;
	@Autowired
	private INewService newService;
	@Autowired
	private IWebsiteService websiteService;
	@Autowired
	private IWebpageService webpageService;
	@Autowired
	private MessageSendingOperations<String> messagingTemplate;
	
	@RequestMapping(value = "/user/website")
	public String index(
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser,
			Model model, HttpSession session)
	{
		
		List<Website> websites = websiteService.getUserWebsites(currentUser);
		model.addAttribute("userWebsites", websites);
		
		
		return "user/website";
	}

	@RequestMapping(value = "/user/websiteAdd", method = RequestMethod.GET)
	public String addWebsite(Model model, HttpSession session)
	{
		model.addAttribute(Constants.MainConstants.WEBSITE, new Website());
		return "user/websiteAdd";
	}

	@RequestMapping(value = "/user/websiteAdd", method = RequestMethod.POST)
	public String addWebsite(
			@ModelAttribute(Constants.MainConstants.WEBSITE) Website website,
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser,
			Model model, HttpSession session)
	{
		website.setUser(currentUser);
		website.setStartObserving(new Date());
		websiteService.createWebsite(website);

		return "redirect:/user/website";
	}

	@RequestMapping(value = "/user/websiteInfo", method = RequestMethod.GET, params = {
			"id" })
	public String infoWebsite(@RequestParam("id") long id, Model model,
			HttpSession session)
	{
		Website webSite = websiteService.getWebsiteById(id);
		List<Webpage> webPages = webpageService.getWebPages(id);
		webSite.setWebPages(webPages);

		model.addAttribute("website", webSite);
		return "user/websiteInfo";
	}

	@RequestMapping(value = "/user/websiteEdit", method = RequestMethod.GET, params = {"id"})
	public String editWebsite(Model model, HttpSession session)
	{

		return "user/websiteManage";
	}

	// TODO: DELETE THIS FUCKING CODE!!!!!!!
	// START WEB-ROBOT-OLD DEPRECATED
	@RequestMapping(value = "/user/website/startRobot", method = RequestMethod.GET)
	public String startRobot(
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser,
			Model model, HttpSession session)
	{
		String webSiteUrl = "http://cpratt.co/";
		Website website = websiteService.getUserWebsite(currentUser,
				webSiteUrl);

		if (website == null)
		{
			website = new Website();
			website.setUrl(webSiteUrl);
			website.setUser(currentUser);
			website.setWebsiteName("C.Pratt's Website/Blog");
			website.setStartObserving(new Date());
			websiteService.createWebsite(website);
		}
/*
		WebRobot webRobot = new WebRobot(website, webpageService,
				websiteService, messagingTemplate, messageService, newService);
		Thread robotThread = new Thread(new RunnableRobot(webRobot, 5000));

		robotThread.start();
*/
		return "redirect:/user/website";
	}

	// START WEB-ROBOT
	@RequestMapping(value = "/user/websiteStartRobot", method = RequestMethod.GET, params = {"id"})
	public String startWebRobot(
			@ModelAttribute(Constants.MainConstants.CURRENTUSERSESSIONID) User currentUser,
			@RequestParam("id") long id, Model model, HttpSession session)
	{
		
/*
		Website website = websiteService.getUserWebsite(currentUser, id);
		WebRobot webRobot = new WebRobot(website, webpageService,
				websiteService, messagingTemplate, messageService, newService);
		Thread robotThread = new Thread(new RunnableRobot(webRobot, 5000));

		robotThread.start();
*/
		return "redirect:/user/website";
	}

}
