package com.andrushko.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Website implements IEntity {

	@Id
	@GeneratedValue
	private Long id;
	private String websiteName;
	private String url; // observe from
	private String domainUrl;
	private Date startObserving;
	@ManyToOne
	private User user;
	
	// Extended analitics
	private String robotsTxt;
	private String siteMapXml;
	private String rss;
	
	@OneToMany
	private List<Webpage> webPages;
	
	public Website() {
		super();
		webPages = new ArrayList<Webpage>();
	}

	public void addWebPage(Webpage webpage)
	{
		webPages.add(webpage);
	}
	
	// GETTERS & SETTERS
	public Long getId() {
		return id;
	}
	public String getWebsiteName() {
		return websiteName;
	}
	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getStartObserving() {
		return startObserving;
	}
	public void setStartObserving(Date startObserving) {
		this.startObserving = startObserving;
	}
	public User getUser()
	{
		return user;
	}
	public void setUser(User user)
	{
		this.user = user;
	}
	public List<Webpage> getWebPages() {
		return webPages;
	}
	public void setWebPages(List<Webpage> webPages) {
		this.webPages = webPages;
	}
	public String getDomainUrl()
	{
		return domainUrl;
	}
	public void setDomainUrl(String domainUrl)
	{
		this.domainUrl = domainUrl;
	}

	public String getRobotsTxt()
	{
		return robotsTxt;
	}

	public void setRobotsTxt(String robotsTxt)
	{
		this.robotsTxt = robotsTxt;
	}

	public String getSiteMapXml()
	{
		return siteMapXml;
	}

	public void setSiteMapXml(String siteMapXml)
	{
		this.siteMapXml = siteMapXml;
	}

	public String getRss()
	{
		return rss;
	}

	public void setRss(String rss)
	{
		this.rss = rss;
	}
	
	
}
