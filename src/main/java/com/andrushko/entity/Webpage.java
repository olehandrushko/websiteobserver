package com.andrushko.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Webpage implements IEntity {

	@Id
	@GeneratedValue
	private Long id;
	
	@Lob
	private String pageSourceHtml;
	
	@Lob
	private String pageSourcePlain;
	
	private String url;

	@ManyToOne
	private Website webSite;
	
	public Long getId() {
		return id;
	}

	public String getPageSourceHtml() {
		return pageSourceHtml;
	}

	public void setPageSourceHtml(String pageSource) {
		this.pageSourceHtml = pageSource;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public Website getWebSite()
	{
		return webSite;
	}

	public void setWebSite(Website webSite)
	{
		this.webSite = webSite;
	}

	public String getPageSourcePlain()
	{
		return pageSourcePlain;
	}

	public void setPageSourcePlain(String pageSourcePlain)
	{
		this.pageSourcePlain = pageSourcePlain;
	}

	
}
