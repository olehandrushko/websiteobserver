package com.andrushko.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User implements IEntity {

	@Id
	@GeneratedValue
	private Long id;

	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String authenticationCode;
	private boolean isAccauntActivated;
	
	@OneToMany
	private List<Website> websites;

	public User() {
		super();
		websites = new ArrayList<Website>();
	}
	public Long getId() {
		return id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Website> getWebsites() {
		return websites;
	}
	public void setWebsites(List<Website> websites) {
		this.websites = websites;
	}
	public String getAuthenticationCode() {
		return authenticationCode;
	}
	public void setAuthenticationCode(String authenticationCode) {
		this.authenticationCode = authenticationCode;
	}
	public boolean isAccauntActivated() {
		return isAccauntActivated;
	}
	public void setAccauntActivated(boolean isAccauntActivated) {
		this.isAccauntActivated = isAccauntActivated;
	}
	
}
