package com.andrushko.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Messange implements IEntity
{

	@Id
	@GeneratedValue
	private Long id;

	private String title;
	@Lob
	private String bodyText;
	@ManyToOne
	private User toUser;
	private Date sendDate;
	
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public User getToUser()
	{
		return toUser;
	}
	public void setToUser(User toUser)
	{
		this.toUser = toUser;
	}
	public Date getSendDate()
	{
		return sendDate;
	}
	public void setSendDate(Date sendDate)
	{
		this.sendDate = sendDate;
	}
	public Long getId()
	{
		return id;
	}
	public String getBodyText()
	{
		return bodyText;
	}
	public void setBodyText(String bodyText)
	{
		this.bodyText = bodyText;
	}
	
	
}
