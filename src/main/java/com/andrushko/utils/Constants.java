package com.andrushko.utils;

public class Constants
{
	public class MainConstants 
	{
		public static final String CURRENTUSERSESSIONID = "currentUser";
		public static final String LAYOUTVM = "layoutVm";
		
		public static final String WEBSITE = "website";
		public static final String URLS = "urls";
		public static final String DOMAINURL = "/WebsiteObserver/";
	}
	
	public class WebRobotConstants
	{
		public static final String ROBOTSTXT = "robots.txt";
		public static final String SITEMAPXML = "Sitemap.xml";
		public static final String RSS = "rss";
	}
	
	public class WebSocketDestinations
	{
		public static final String ANALYZE_WEBSITE = "/topic/analyzeWebsite";
	}

	public class NotificationType
	{
		public static final int SUCCESS = 1;
		public static final int WARNING = 2;
		public static final int ERROR = 3;		
	}

}
