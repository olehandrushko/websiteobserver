package com.andrushko.utils;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

public class StringUtils {
    public static String concat(Object... values) {
        StringBuilder output = new StringBuilder();
        for (Object object : values) {
            output.append(object);
        }
        return output.toString();
    }
    
    public static String buildAbsoluteUrl(String domain, String url){
    	String returnUrl = null;
    	try
		{
			final URI u = new URI(url);
			if(u.isAbsolute()){
				returnUrl = u.toString();
			} else {
				if(domain.endsWith("/") && url.startsWith("/")){
					returnUrl = domain + url.substring(1);
				} else{
					returnUrl = domain + url;
				}
			}
		} catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
    	return returnUrl;
    } 

    public static String getURLWithContextPath(HttpServletRequest request) {
	   return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}
}