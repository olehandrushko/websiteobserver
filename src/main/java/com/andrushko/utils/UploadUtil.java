package com.andrushko.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.andrushko.model.FileUpload;
import com.andrushko.model.FileUploadResult;

public class UploadUtil
{
	private String rootLocation;
	private FileUpload fileUpload;
	private FileUpload[] fileUploads;

	public UploadUtil()
	{
		super();
		rootLocation = System.getProperty("catalina.home");
	}

	public UploadUtil(FileUpload fileUpload)
	{
		this();
		this.fileUpload = fileUpload;

	}

	public UploadUtil(FileUpload[] fileUploads)
	{
		this();
		this.fileUploads = fileUploads;
	}

	public FileUploadResult upload()
	{
		if (fileUpload != null)
		{
			uploadFile(fileUpload);
		}
		if (fileUploads != null && fileUploads.length > 0)
		{
			for (int i = 0; i < fileUploads.length; i++)
			{
				if (fileUploads[i] != null)
				{
					uploadFile(fileUpload);
				}
			}
		}
		return FileUploadResult.SUCCESS;
	}

	private void uploadFile(FileUpload fileUpload)
	{
		if (fileUpload.getFile() == null)
		{
			// TODO: throw some Exception
		}
		MultipartFile file = fileUpload.getFile();
		String fileName = file.getOriginalFilename();
		String fileLocation = getFileLocation(fileUpload);
		try
		{
			byte[] bytes = file.getBytes();
			File dir = new File(fileLocation);
			if (dir.exists() || (!dir.exists() && dir.mkdirs()))
			{
				File serverFile = new File(getAbsolutePath(dir, fileName));
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	private String getFileLocation(FileUpload fileUploadParam)
	{
		StringBuilder returnPath = new StringBuilder(rootLocation);
		String returnPathValue = null;
		if (fileUploadParam != null)
		{
			if (fileUploadParam.getUser() != null)
			{
				returnPath.append(File.separator) // "/"
						.append(fileUploadParam.getUser().getEmail())
						.append(File.separator);
			}
			if (fileUploadParam.getWebSite() != null)
			{
				returnPath.append(File.separator) // "/"
						.append("website-")
						.append(fileUploadParam.getWebSite().getId())
						.append(File.separator);
			}
			returnPathValue = returnPath.toString();
			if (!returnPathValue.equals(rootLocation))
			{
				return returnPathValue.toString();
			}
		}
		return null;
	}

	private String getAbsolutePath(File file, String name)
	{
		return file != null ? new StringBuilder(file.getAbsolutePath())
				.append(File.separator).append(name).toString() : null;
	}

	// GETTERS & SETTERS
	public FileUpload getFileUpload()
	{
		return fileUpload;
	}

	public void setFileUpload(FileUpload fileUpload)
	{
		this.fileUpload = fileUpload;
	}

	public String getRootLocation()
	{
		return rootLocation;
	}

	public FileUpload[] getFileUploads()
	{
		return fileUploads;
	}

	public void setFileUploads(FileUpload[] fileUploads)
	{
		this.fileUploads = fileUploads;
	}

}
