package com.andrushko.utils;

import javax.mail.*;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Properties;

public class MailUtils {
	
	private static Logger log = Logger.getLogger(MailUtils.class);
	private static String SMPT_HOSTNAME = "smtp.gmail.com";
	private static String USERNAME = "website.observer94@gmail.com";
	private static String PASSWORD = "observer94";
	private static String PORT = "465";
	//private static String REGISTER = "sbakdjfsb";
	private static String AUTENTIFICATION = "asdasd";
	
	private Properties props;
	private Session session;

    public MailUtils() {
        props = new Properties();
        props.put("mail.smtp.host", SMPT_HOSTNAME);
        props.put("mail.from", USERNAME);
        props.put("mail.smtp.socketFactory.port", PORT);
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.port", PORT);
        props.put("mail.smtp.auth", "true");
        session = Session.getInstance(getProps(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });
    }
	
	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}
	
	public void sendMessage(String email, String messageText){

	    try {
	        MimeMessage msg = new MimeMessage(session);
	        msg.setFrom();
	        msg.setSubject(AUTENTIFICATION);
	        msg.setSentDate(new java.util.Date());
	        msg.setText(messageText);
	        msg.setRecipients(Message.RecipientType.TO,
	                          email);
            Transport.send(msg);

	     } catch (MessagingException mex) {
	        log.error("Sending is fail: " + mex);
	     }
	}

	public void sendMessage(List<String> emails, String message, String subject){

	    try {
	        MimeMessage msg = new MimeMessage(session);
	        msg.setFrom();
	        msg.setSubject(subject);
	        msg.setSentDate(new java.util.Date());
	        msg.setText(message);
	        for (String email : emails) {
	        	msg.setRecipients(Message.RecipientType.TO,
                        email);
	        	Transport.send(msg);
			}
	     } catch (MessagingException mex) {
	    	 log.error("Sending is fail: " + mex);
	     }
	}
}
