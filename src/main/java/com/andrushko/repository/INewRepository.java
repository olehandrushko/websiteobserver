package com.andrushko.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.andrushko.entity.New;

@Repository("NewRepository")
public interface INewRepository extends JpaRepository<New, Long>
{
	@Query("SELECT n FROM New n WHERE n.user.id = ?1 order by n.date")
	List<New> findByUserId(Long id);
	
	@Query("SELECT n FROM New n WHERE n.user.email = ?1 order by n.date")
	List<New> findByEmailAddress(String emailAddress);
}
