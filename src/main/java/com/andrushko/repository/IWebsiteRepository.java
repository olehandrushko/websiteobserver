package com.andrushko.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.andrushko.entity.Website;

//LEFT JOIN FETCH w.webPages ---SUKA

@Repository("WebsiteRepository")
public interface IWebsiteRepository extends JpaRepository<Website, Long>{

	@Query("SELECT w FROM Website w WHERE w.user.email = ?1")
	List<Website> findByEmailAddress(String emailAddress);
	
	@Query("SELECT w FROM Website w WHERE w.user.email = ?1 AND w.url = ?2")
	Website findByEmailAndUrl(String emailAddress, String url);
	
	@Query("SELECT w FROM Website w WHERE w.user.email = ?1 AND w.id = ?2")
	Website findByEmailAndId(String emailAddress, Long id);
	
	@Query("SELECT w FROM Website w WHERE w.id = ?1")
	Website findById(Long id);
}
