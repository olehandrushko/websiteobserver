package com.andrushko.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.andrushko.entity.Messange;

@Repository("MessageRepository")
public interface IMessageRepository extends JpaRepository<Messange, Long>
{
	@Query("SELECT m FROM Messange m WHERE m.toUser.id = ?1 order by m.sendDate")//order by m.sendDate
	List<Messange> findByUserId(Long id);

	@Query("SELECT m FROM Messange m WHERE m.toUser.email = ?1 order by m.sendDate")
	List<Messange> findByEmailAddress(String emailAddress);
}
