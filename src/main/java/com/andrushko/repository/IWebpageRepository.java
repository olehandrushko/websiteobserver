package com.andrushko.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.andrushko.entity.Webpage;

@Repository("WebpageRepository")
public interface IWebpageRepository extends JpaRepository<Webpage, Long>{
	@Query("SELECT w FROM Webpage w WHERE w.url = ?1")
	Webpage findByWebpageUrl(String url);
	
	@Query("SELECT w FROM Webpage w WHERE w.url = ?1 AND w.webSite.id = ?2")
	Webpage findWebsiteWebpageByUrl(String url, long webSiteId);
	
	@Query("SELECT w FROM Webpage w WHERE w.webSite.id = ?1")
	List<Webpage> findAllWebsiteWebpages(long websiteId);
}
