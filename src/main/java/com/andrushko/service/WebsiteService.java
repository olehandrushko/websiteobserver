package com.andrushko.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.andrushko.entity.User;
import com.andrushko.entity.Website;
import com.andrushko.repository.IWebsiteRepository;

@Service("WebsiteService")
public class WebsiteService implements IWebsiteService
{

	@Autowired
	private IWebsiteRepository websiteRepository;

	public void createWebsite(Website website)
	{
		if (website != null && !website.getUrl().isEmpty())
		{
			String domainUrl = getWebSiteDomainUrl(website);
			if (domainUrl != null && domainUrl.equals(website.getUrl()))
			{
				website.setDomainUrl(domainUrl);
			} else
			{
				website.setDomainUrl(website.getUrl());
			}
			websiteRepository.saveAndFlush(website);
		}
	}

	public void saveWebsite(Website website)
	{
		websiteRepository.saveAndFlush(website);
	}

	public List<Website> getUserWebsites(User user)
	{
		return websiteRepository.findByEmailAddress(user.getEmail());
	}

	public Website geWebsiteById(long id)
	{
		return websiteRepository.findById(id);
	}

	public Website getUserWebsite(User user, String webSiteUrl)
	{
		return websiteRepository.findByEmailAndUrl(user.getEmail(), webSiteUrl);
	}

	public String getWebSiteDomainUrl(Website website)
	{
		URI uri = null;
		try
		{
			uri = new URI(website.getUrl());
		} catch (URISyntaxException e)
		{
			e.printStackTrace();
		}
		if (uri == null)
			return null;
		String domain = uri.getHost();
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}

	
	public Website getUserWebsite(User user, long id)
	{
		return websiteRepository.findByEmailAndId(user.getEmail(), id);
	}

	public Website getWebsiteById(long id)
	{
		return websiteRepository.findById(id);
	}
}
