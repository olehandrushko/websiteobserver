package com.andrushko.service;

import java.util.List;

import com.andrushko.entity.Webpage;

public interface IWebpageService
{
	void saveWebpage(Webpage webpage);

	Webpage getWebPage(String url);

	Webpage getWebPage(String url, long id);

	List<Webpage> getWebPages(long websiteId);

	Webpage getWebPage(List<Webpage> webPages, long id);

	void remove(Webpage dbPageToRemove);
}
