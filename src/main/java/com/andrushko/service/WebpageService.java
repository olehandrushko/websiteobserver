package com.andrushko.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.andrushko.entity.Webpage;
import com.andrushko.repository.IWebpageRepository;

@Service("WebpageService")
public class WebpageService implements IWebpageService
{
	@Autowired
	private IWebpageRepository webpageRepository;

	public void saveWebpage(Webpage webpage)
	{
		if (webpage != null && !webpage.getUrl().isEmpty())
		{
			webpageRepository.saveAndFlush(webpage);
		}
	}

	public Webpage getWebPage(String url)
	{
		return webpageRepository.findByWebpageUrl(url);
	}

	public Webpage getWebPage(String url, long id)
	{
		return webpageRepository.findWebsiteWebpageByUrl(url, id);
	}

	public List<Webpage> getWebPages(long websiteId)
	{
		return webpageRepository.findAllWebsiteWebpages(websiteId);
	}

	
	public Webpage getWebPage(List<Webpage> webPages, long id)
	{
		for (Webpage webpage : webPages)
		{
			if(webpage.getId() == id)
			{
				return webpage;
			}
		}
		return null;
	}

	public void remove(Webpage dbPageToRemove)
	{
		webpageRepository.delete(dbPageToRemove);
		webpageRepository.flush();
	}
}
