package com.andrushko.service;

import java.util.List;

import com.andrushko.entity.New;
import com.andrushko.entity.User;

public interface INewService
{
	List<New> getUserNews(User user);
}
