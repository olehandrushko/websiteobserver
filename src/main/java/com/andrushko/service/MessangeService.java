package com.andrushko.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.andrushko.entity.Messange;
import com.andrushko.entity.User;
import com.andrushko.repository.IMessageRepository;

@Service("MessangeService")
public class MessangeService implements IMessangeService
{
	@Autowired
	private IMessageRepository messageRepository;
	
	public List<Messange> getUserMessages(User user)
	{
		return messageRepository.findByEmailAddress(user.getEmail());
	}

	public void saveMessange(Messange messange)
	{
		messageRepository.saveAndFlush(messange);		
	}
}
