package com.andrushko.service;

import java.util.List;

import com.andrushko.entity.Messange;
import com.andrushko.entity.User;

public interface IMessangeService
{
	List<Messange> getUserMessages(User user);
	
	void saveMessange(Messange messange);
}
