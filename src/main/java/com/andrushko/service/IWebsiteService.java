package com.andrushko.service;

import java.util.List;

import com.andrushko.entity.User;
import com.andrushko.entity.Website;

public interface IWebsiteService
{
	void createWebsite(Website website);

	void saveWebsite(Website website);

	Website getUserWebsite(User user, String webSiteUrl);

	Website getUserWebsite(User user, long id);

	Website getWebsiteById(long id);

	List<Website> getUserWebsites(User user);

	String getWebSiteDomainUrl(Website website);
}
