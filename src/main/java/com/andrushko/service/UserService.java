package com.andrushko.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.andrushko.entity.User;
import com.andrushko.repository.IUserRepository;
import com.andrushko.utils.EncryptionUtils;

@Service("UserService")
public class UserService implements IUserService
{

	@Autowired
	private IUserRepository userRepository;

	public List<User> getUsers()
	{
		return userRepository.findAll();
	}

	public void registerUser(User user)
	{
		try
		{
			String passwordHash = EncryptionUtils
					.createHash(user.getPassword());
			user.setPassword(passwordHash);
			user.setAccauntActivated(false);
			user.setAuthenticationCode(EncryptionUtils.createHash(user.getEmail()));
		} catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		} catch (InvalidKeySpecException e)
		{
			e.printStackTrace();
		}
		userRepository.saveAndFlush(user);
	}

	public boolean isValidCreadentials(String email, String password)
	{
		User dbUser = getUser(email);
		try
		{
			if (dbUser != null && EncryptionUtils.validatePassword(password,
					dbUser.getPassword()))
			{
				return true;
			}
		} catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		} catch (InvalidKeySpecException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public void updateUser(User user)
	{
		userRepository.saveAndFlush(user);
	}

	public User getUser(String email)
	{
		return userRepository.findByEmailAddress(email);
	}

}
