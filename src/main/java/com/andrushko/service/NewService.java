package com.andrushko.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.andrushko.entity.New;
import com.andrushko.entity.User;
import com.andrushko.repository.INewRepository;

@Service("NewService")
public class NewService implements INewService
{
	@Autowired
	private INewRepository newRepository;
	
	
	public List<New> getUserNews(User user)
	{
		return newRepository.findByUserId(user.getId());
	}

}
