package com.andrushko.service;

import java.util.List;

import com.andrushko.entity.User;

public interface IUserService
{
	List<User> getUsers();

	void registerUser(User user);

	void updateUser(User user);

	User getUser(String email);

	boolean isValidCreadentials(String email, String password);
}
