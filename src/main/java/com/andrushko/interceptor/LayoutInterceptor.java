package com.andrushko.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.andrushko.entity.User;
import com.andrushko.entity.Webpage;
import com.andrushko.entity.Website;
import com.andrushko.service.IMessangeService;
import com.andrushko.service.INewService;
import com.andrushko.service.IWebpageService;
import com.andrushko.service.IWebsiteService;
import com.andrushko.utils.Constants;
import com.andrushko.viewmodel.LayoutViewModel;

public class LayoutInterceptor extends HandlerInterceptorAdapter
{
	@Autowired
	private IWebsiteService websiteService;
	@Autowired
	private IWebpageService webpageService;
	@Autowired
	private IMessangeService messangeService;
	@Autowired
	private INewService newService;

	private LayoutViewModel layoutVm;

	public LayoutInterceptor()
	{
		super();
		layoutVm = new LayoutViewModel();
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception
	{
		HttpSession session = request.getSession();
		User userData = (User) session.getAttribute(Constants.MainConstants.CURRENTUSERSESSIONID);

		if (userData != null)
		{
			List<Website> websites = websiteService.getUserWebsites(userData); 
			List<Webpage> webpages = new ArrayList<Webpage>();
			layoutVm.setWebsites(websites);
			for (Website website : websites)
			{
				webpages.addAll(webpageService.getWebPages(website.getId()));
			}
			layoutVm.setWebpages(webpages);
			layoutVm.setNews(newService.getUserNews(userData));
			layoutVm.setMessanges(messangeService.getUserMessages(userData));
		}
		session.setAttribute("layoutVm", layoutVm);
		
		return super.preHandle(request, response, handler);
	}
}
