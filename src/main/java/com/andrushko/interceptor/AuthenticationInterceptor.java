package com.andrushko.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.andrushko.annotation.Authenticate;
import com.andrushko.entity.User;
import com.andrushko.utils.Constants;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter
{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		if (handler instanceof HandlerMethod)
		{
			HandlerMethod method = (HandlerMethod) handler;
			boolean isControllerSecured = method.getMethod().getDeclaringClass()
					.isAnnotationPresent(Authenticate.class);
			boolean isMethodSecured = method.getMethod()
					.isAnnotationPresent(Authenticate.class);

			if (isControllerSecured || isMethodSecured)
			{
				System.out.println(
						"Controller or ActionMethod is secured from UnAuthorizedAccess");
				return AuthorizationHandler(request, response);
			}
		}
		return true;
	}

	private boolean AuthorizationHandler(HttpServletRequest request,
			HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		User userData = (User) session.getAttribute(Constants.MainConstants.CURRENTUSERSESSIONID);

		if (userData == null)
		{
			try
			{
				response.sendRedirect(Constants.MainConstants.DOMAINURL);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			return false;
		}
		return true;
	}

}
