package com.andrushko.viewmodel;

import java.util.List;

import com.andrushko.entity.Messange;
import com.andrushko.entity.New;
import com.andrushko.entity.Webpage;
import com.andrushko.entity.Website;

public class UserHomeViewModel
{
	private List<Website> websites;
	private List<Webpage> webpages;
	private List<New> news;
	private List<Messange> messanges;
	
	public List<Website> getWebsites()
	{
		return websites;
	}
	public void setWebsites(List<Website> websites)
	{
		this.websites = websites;
	}
	public List<Webpage> getWebpages()
	{
		return webpages;
	}
	public void setWebpages(List<Webpage> webpages)
	{
		this.webpages = webpages;
	}
	public List<New> getNews()
	{
		return news;
	}
	public void setNews(List<New> news)
	{
		this.news = news;
	}
	public List<Messange> getMessanges()
	{
		return messanges;
	}
	public void setMessanges(List<Messange> messanges)
	{
		this.messanges = messanges;
	}
	
	
	
}
