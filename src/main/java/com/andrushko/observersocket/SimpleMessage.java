package com.andrushko.observersocket;

public class SimpleMessage {

	private String message;

	public SimpleMessage(String string)
	{
		this.message = string;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
