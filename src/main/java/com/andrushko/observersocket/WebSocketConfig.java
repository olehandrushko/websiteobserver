package com.andrushko.observersocket;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver;
import org.springframework.messaging.handler.invocation.HandlerMethodReturnValueHandler;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

@Configuration
@EnableWebSocketMessageBroker
@EnableScheduling
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/simplemessages").withSockJS();
        System.out.println("***************registerStompEndpoints**************");
    }

    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic/", "/queue/");
        config.setApplicationDestinationPrefixes("/app");
        System.out.println("***************configureMessageBroker**************");
    }

    public void configureClientInboundChannel(ChannelRegistration registration) {
    	System.out.println("***************configureClientInboundChannel**************");
    }

    public void configureClientOutboundChannel(ChannelRegistration registration) {
        registration.taskExecutor().corePoolSize(4).maxPoolSize(10);
        System.out.println("***************configureClientOutboundChannel**************");
    }

	public void configureWebSocketTransport(WebSocketTransportRegistration registry) {
		System.out.println("***************configureWebSocketTransport**************");
		
	}

	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		System.out.println("***************addArgumentResolvers**************");
		
	}

	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
		System.out.println("***************addReturnValueHandlers**************");
		
	}

	public boolean configureMessageConverters(List<MessageConverter> messageConverters) {
		System.out.println("***************configureMessageConverters**************");
		return true;
	}
}
