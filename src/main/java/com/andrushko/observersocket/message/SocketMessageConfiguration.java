package com.andrushko.observersocket.message;

public class SocketMessageConfiguration
{
	public static class Colors
	{
		public static final String RED = "#ff0000";
		public static final String GREEN = "#00ff00";
		public static final String BLUE = "#0000ff";
		
		public static final String LIGHT_GRAY = "#d3d3d3";
		public static final String DARK_SEA_GREEN = "#6CC88F";
		public static final String VIOLET = "#ee82ee";
	}
	
	public static class Icons
	{
		public static final String ARROW_RIGHT = "fa fa-arrow-right";
		public static final String PAGE_UPDATE = "fa fa-bullhorn";
		public static final String NEW_PAGE = "fa fa-plus";
		public static final String DELETE_PAGE = "fa fa-minus";
		public static final String EXCLAMATION = "fa fa-exclamation";
	} 
}
