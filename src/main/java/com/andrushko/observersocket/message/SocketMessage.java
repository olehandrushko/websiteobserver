package com.andrushko.observersocket.message;

public class SocketMessage {

    private String messageText;
    private String messageColor;
    private String messageIcon;
    private String messageIconColor;

	public SocketMessage()
	{
		this("Just empty message! Don't worry, everything is ok :)");
	}
	public SocketMessage(String messageText)
	{
		this(messageText, SocketMessageConfiguration.Colors.LIGHT_GRAY);
	}
	public SocketMessage(String messageText, String messageColor)
	{
		this(messageText, messageColor, SocketMessageConfiguration.Icons.ARROW_RIGHT);
	}
	public SocketMessage(String messageText, String messageColor,
			String messageIcon)
	{
		this(messageText, messageColor, messageIcon, SocketMessageConfiguration.Colors.LIGHT_GRAY);
	}
	public SocketMessage(String messageText, String messageColor,
			String messageIcon, String messageIconColor)
	{
		super();
		this.messageText = messageText;
		this.messageColor = messageColor;
		this.messageIcon = messageIcon;
		this.messageIconColor = messageIconColor;
	}
	
	public String getMessageText()
	{
		return messageText;
	}
	public void setMessageText(String messageText)
	{
		this.messageText = messageText;
	}
	public String getMessageColor()
	{
		return messageColor;
	}
	public void setMessageColor(String messageColor)
	{
		this.messageColor = messageColor;
	}
	public String getMessageIcon()
	{
		return messageIcon;
	}
	public void setMessageIcon(String messageIcon)
	{
		this.messageIcon = messageIcon;
	}
	public String getMessageIconColor()
	{
		return messageIconColor;
	}
	public void setMessageIconColor(String messageIconColor)
	{
		this.messageIconColor = messageIconColor;
	}
}
