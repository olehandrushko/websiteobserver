package com.andrushko.observersocket;

import org.springframework.messaging.core.MessageSendingOperations;

import com.andrushko.observersocket.message.SocketMessage;

public class SocketMessanger
{
	private String analyzeEndpoint;
	private MessageSendingOperations<String> messagingTemplate;
	private SocketMessage socketMessage;
		
	public SocketMessanger(String analyzeEndpoint, MessageSendingOperations<String> messagingTemplate)
	{
		super();
		this.analyzeEndpoint = analyzeEndpoint;
		this.messagingTemplate = messagingTemplate;
		this.socketMessage = new SocketMessage();
	}

	public void sendMessage(SocketMessage socketMessage)
	{
		messagingTemplate.convertAndSend(analyzeEndpoint, socketMessage);
	}
	
	public void sendMessage(String message)
	{
		socketMessage.setMessageText(message);
		messagingTemplate.convertAndSend(analyzeEndpoint, socketMessage);
	}
	
	// GETTERS & SETTERS
	public String getAnalyzeEndpoint()
	{
		return analyzeEndpoint;
	}

	public void setAnalyzeEndpoint(String analyzeEndpoint)
	{
		this.analyzeEndpoint = analyzeEndpoint;
	}
}
