package com.andrushko.enums;

public enum UserType
{
	ADMIN,
	SIMPLE_USER
}
