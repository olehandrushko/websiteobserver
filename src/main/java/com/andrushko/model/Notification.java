package com.andrushko.model;

import com.andrushko.utils.Constants;

public class Notification
{
	private String message;
	private String icon;
	private boolean isAvailable;
	private static  Notification instance;
	private int bgType;
	
	
	private Notification()
	{
	}

	public static Notification getInstance()
	{
		if(instance == null)
		{
			instance  = new Notification();
		}
		return instance;
	}

	public static void build(String message, String icon)
	{
		getInstance().setMessage(message);
		getInstance().setIcon(icon);
		getInstance().setAvailable(true);
		getInstance().setBgType(Constants.NotificationType.WARNING);
	}

	public static void build(String message, String icon, int bgType)
	{
		getInstance().setMessage(message);
		getInstance().setIcon(icon);
		getInstance().setBgType(bgType);
		getInstance().setAvailable(true);
	}

	
	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public boolean isAvailable()
	{
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable)
	{
		this.isAvailable = isAvailable;
	}

	public int getBgType()
	{
		return bgType;
	}

	public void setBgType(int bgType)
	{
		this.bgType = bgType;
	}


}
