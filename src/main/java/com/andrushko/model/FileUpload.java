package com.andrushko.model;

import org.springframework.web.multipart.MultipartFile;

import com.andrushko.entity.User;
import com.andrushko.entity.Website;

public class FileUpload
{
	private MultipartFile file;
	private User user;
	private Website webSite;
	
	public FileUpload()
	{
		super();
	}

	public FileUpload(MultipartFile file)
	{
		this(file, null, null);
	}
	
	public FileUpload(MultipartFile file, User user) 
	{
		 this(file, user, null);
	}

	public FileUpload(MultipartFile file, User user, Website webSite)
	{
		super();
		this.file = file;
		this.user = user;
		this.webSite = webSite;
	}

	public MultipartFile getFile()
	{
		return file;
	}

	public void setFile(MultipartFile file)
	{
		this.file = file;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Website getWebSite()
	{
		return webSite;
	}

	public void setWebSite(Website webSite)
	{
		this.webSite = webSite;
	}
	
	
}