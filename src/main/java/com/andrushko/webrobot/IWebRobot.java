package com.andrushko.webrobot;

import java.io.IOException;

public interface IWebRobot {
	String getUrlSource(String urlString) throws IOException;

	void startAnalyzing(int timeout); 
}
