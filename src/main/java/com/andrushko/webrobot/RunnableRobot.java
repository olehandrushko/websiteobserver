package com.andrushko.webrobot;

public class RunnableRobot implements IRunnableRobot
{
	private IWebRobot webRobot;
	private int runningTimeout;

	public RunnableRobot(IWebRobot webRobot, int runningTimeout)
	{
		super();
		this.webRobot = webRobot;
		this.runningTimeout = runningTimeout;	
	}

	public void run()
	{
		webRobot.startAnalyzing(runningTimeout);				
	}

	
	public IWebRobot getWebRobot()
	{
		return webRobot;
	}

	public void setWebRobot(IWebRobot webRobot)
	{
		this.webRobot = webRobot;
	}


}
