package com.andrushko.webrobot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

import org.jsoup.Jsoup;
import org.springframework.messaging.core.MessageSendingOperations;

import com.andrushko.entity.Messange;
import com.andrushko.entity.User;
import com.andrushko.entity.Webpage;
import com.andrushko.entity.Website;
import com.andrushko.observersocket.SocketMessanger;
import com.andrushko.observersocket.message.SocketMessage;
import com.andrushko.observersocket.message.SocketMessageConfiguration;
import com.andrushko.service.IMessangeService;
import com.andrushko.service.INewService;
import com.andrushko.service.IWebpageService;
import com.andrushko.service.IWebsiteService;
import com.andrushko.utils.Constants;
import com.andrushko.utils.MailUtils;
import com.andrushko.utils.StringUtils;

public class WebRobot implements IWebRobot
{
	private Website website;
	private IWebpageService webpageService;
	private IMessangeService messangeService;
	private INewService newService;
	private User currentUser;
	
	//private IWebsiteService websiteService;
	private SocketMessanger socketMessanger;
	private MailUtils mailUtil;

	public WebRobot(Website website, User currentUser, IWebpageService webpageService,
			IWebsiteService websiteService,
			MessageSendingOperations<String> messagingTemplate,
			IMessangeService messangeService, INewService newService)
	{
		super();
		this.mailUtil = new MailUtils();
		this.website = website;
		this.webpageService = webpageService;
		this.currentUser = currentUser;
		this.messangeService = messangeService;
		this.newService = newService;
		//this.websiteService = websiteService;
		this.socketMessanger = new SocketMessanger(
				Constants.WebSocketDestinations.ANALYZE_WEBSITE,
				messagingTemplate);
	}

	public void startAnalyzing(int timeout)
	{
		primaryProcess();
		socketMessanger.sendMessage(" I am starting processing pages for " + website.getUrl());

		List<String> newWebPagesUrls = new ArrayList<String>();
		processPage(website, website.getUrl(), newWebPagesUrls);
		newPagesSendMessanges(newWebPagesUrls);
		
		socketMessanger.sendMessage(" I've just ended looking for new pages for your website " + website.getUrl());
		
		socketMessanger.sendMessage(" I am starting processing any updates " + website.getUrl());
		processPagesUpdates(website,
				webpageService.getWebPages(website.getId()));

		socketMessanger.sendMessage(" I've just ended observing for updates for " + website.getUrl());
	}
	
	public void processPage(Website website, String url,
			List<String> newWebPagesUrls)
	{
		if (newWebPagesUrls.contains(url))
		{
			return;
		}

		Webpage page = webpageService.getWebPage(url, website.getId());
		if (page == null)
		{
			String pageSource = null;
			try
			{
				pageSource = getUrlSource(url);
			} catch (IOException e)
			{
			}

			if (pageSource != null)
			{
				String plainTextPageSource = getBodyTextFromHtml(pageSource);
				page = new Webpage();
				page.setUrl(url);
				page.setPageSourceHtml(pageSource);
				page.setPageSourcePlain(plainTextPageSource);
				page.setWebSite(website);
				webpageService.saveWebpage(page);
				newWebPagesUrls.add(page.getUrl());

				String message = StringUtils.concat(" Webpage added: ", page.getUrl());
				socketMessanger.sendMessage(new SocketMessage(message,
						SocketMessageConfiguration.Colors.DARK_SEA_GREEN,
						SocketMessageConfiguration.Icons.NEW_PAGE,
						SocketMessageConfiguration.Colors.DARK_SEA_GREEN));
				

				List<String> anchors = null;
				try
				{
					anchors = getAnchorTags(page.getPageSourceHtml());
				} catch (IOException e)
				{
				}

				if (anchors != null)
				{
					for (String link : anchors)
					{
						if (link != null)
						{
							processPage(website, link, newWebPagesUrls);
						}
					}
				}
			}
		}
	}

	public void processPagesAddAndRemoves(String url)
	{
		//boolean isWebpagesDeleted
		List<String> currentWebsiteLinks = getCurrentLinks(url, website, new ArrayList<String>());
		List<String> dbPagesUrls = getDbWebpagesUrls(website);
		//removingold pages
		for (String oneDbCurrent : dbPagesUrls)
		{
			if(!currentWebsiteLinks.contains(oneDbCurrent)){
				Webpage dbPageToRemove = webpageService.getWebPage(oneDbCurrent);
				webpageService.remove(dbPageToRemove);
				String message = StringUtils.concat(" Webpage deleted: ", oneDbCurrent);
				socketMessanger.sendMessage(new SocketMessage(message,
						SocketMessageConfiguration.Colors.RED,
						SocketMessageConfiguration.Icons.DELETE_PAGE,
						SocketMessageConfiguration.Colors.RED));
			}
		}
		
		//adding new pages
		for (String oneCurrent : currentWebsiteLinks)
		{
			if(!dbPagesUrls.contains(oneCurrent)){
				String pageSource = null;
			
				Webpage page = new Webpage();
				try
				{
					pageSource = getUrlSource(oneCurrent);
				} catch (IOException e)
				{
				}

				if (pageSource != null)
				{
					String plainTextPageSource = getBodyTextFromHtml(pageSource);
					page = new Webpage();
					page.setUrl(oneCurrent);
					page.setPageSourceHtml(pageSource);
					page.setPageSourcePlain(plainTextPageSource);
					page.setWebSite(website);
					webpageService.saveWebpage(page);
					
					String message = StringUtils.concat(" Webpage added: ", page.getUrl());
					socketMessanger.sendMessage(new SocketMessage(message,
							SocketMessageConfiguration.Colors.DARK_SEA_GREEN,
							SocketMessageConfiguration.Icons.NEW_PAGE,
							SocketMessageConfiguration.Colors.DARK_SEA_GREEN));
				}
			}
		}
	}
	
	public void processPagesUpdates(Website website,
			List<Webpage> dbWebsitePages)
	{
		List<Webpage> newWebsitePages = webpageService
				.getWebPages(website.getId());
		for (Webpage newWebpage : newWebsitePages)
		{
			String pageSource = null;
			try
			{
				pageSource = getUrlSource(newWebpage.getUrl());
			} catch (IOException e)
			{
			}
			if (pageSource == null)
			{
				return;
			}
			newWebpage.setPageSourceHtml(pageSource);
			newWebpage.setPageSourcePlain(getBodyTextFromHtml(pageSource));

			Webpage oldPage = webpageService.getWebPage(dbWebsitePages,
					newWebpage.getId());
			if (oldPage != null && !oldPage.getPageSourcePlain()
					.equals(newWebpage.getPageSourcePlain()))
			{
				String message = StringUtils.concat(" Webpage updates on: ",
						newWebpage.getUrl());
				socketMessanger.sendMessage(new SocketMessage(message,
						SocketMessageConfiguration.Colors.VIOLET,
						SocketMessageConfiguration.Icons.PAGE_UPDATE,
						SocketMessageConfiguration.Colors.VIOLET));
				
				oldPage.setPageSourceHtml(newWebpage.getPageSourceHtml());
				oldPage.setPageSourcePlain(newWebpage.getPageSourcePlain());
				webpageService.saveWebpage(oldPage);
			}
		}
		threadSleep(5000);

		processPagesAddAndRemoves(website.getUrl());
		//newPagesSendMessanges(new ArrayList<String>());
		processPagesUpdates(website, newWebsitePages);
	}

	
	private List<String> getCurrentLinks(String url, Website website, List<String> init){
		if (init.contains(url))
		{
			return init;
		} else {
			init.add(url);
		}
		String pageSource = null;
		try
		{
			pageSource = getUrlSource(url);
		} catch (IOException e)
		{
		}
		if(pageSource != null){
			List<String> anchors = null;
			try
			{
				anchors = getAnchorTags(pageSource);
			} catch (IOException e)
			{
			}

			if (anchors != null)
			{
				for (String link : anchors)
				{
					if (link != null)
					{
						getCurrentLinks(link, website, init);
					}
				}
			}
		}
		return init;
	} 
	
	private void newPagesSendMessanges(List<String> newWebPagesUrls)
	{
		if(newWebPagesUrls.size() > 0){
			StringBuilder messangeBody = new StringBuilder("New pages:\n");
			for (String webpage : newWebPagesUrls)
			{
				messangeBody.append(webpage).append("\n");
			}
			Messange messange = new Messange();
			messange.setSendDate(new Date());
			messange.setTitle("There are some new pages on " + website.getUrl());
			messange.setToUser(currentUser);
			messange.setBodyText(messangeBody.toString());
			
			messangeService.saveMessange(messange);
			mailUtil.sendMessage(currentUser.getEmail(), "Some new pages are added!\n" + messange.getBodyText());
		}
	}
	
	public List<String> getAnchorTags(String htmlSource) throws IOException
	{
		Reader reader = new StringReader(htmlSource);
		HTMLEditorKit.Parser parser = new ParserDelegator();
		final List<String> links = new ArrayList<String>();

		parser.parse(reader, new HTMLEditorKit.ParserCallback()
		{
			public void handleStartTag(HTML.Tag t, MutableAttributeSet a,
					int pos)
			{
				if (t == HTML.Tag.A)
				{
					Object link = a.getAttribute(HTML.Attribute.HREF);
					if (link != null)
					{
						String linkValue = String.valueOf(link);
						if (!linkValue.contains("#")
								&& !linkValue.startsWith("javascript:"))
						{
							links.add(String.valueOf(link));
						}
					}
				}
			}
		}, true);

		reader.close();

		for (int i = 0; i < links.size(); i++)
		{
			links.set(i, StringUtils.buildAbsoluteUrl(website.getDomainUrl(),
					links.get(i)));
			if (!links.get(i).startsWith(website.getDomainUrl()))
			{
				links.set(i, null);
			}
		}

		return links;
	}

	private List<String> getDbWebpagesUrls(Website w) {
		List<String> dbWebPagesUrls = new ArrayList<String>();
		List<Webpage> newPages = webpageService.getWebPages(w.getId());
		for (Webpage webpage : newPages)
		{
			dbWebPagesUrls.add(webpage.getUrl());
		}
		return dbWebPagesUrls;	
	}
	public String getUrlSource(String url) throws IOException
	{
		URL yahoo = new URL(url);
		URLConnection yc = yahoo.openConnection();

		HttpURLConnection urlc = (HttpURLConnection) yahoo.openConnection();
		urlc.setAllowUserInteraction(false);
		urlc.setDoInput(true);
		urlc.setDoOutput(false);
		urlc.setUseCaches(true);
		urlc.setRequestMethod("HEAD");
		urlc.connect();
		String mime = urlc.getContentType();
		if (!mime.contains("text/html"))
		{
			throw new IOException("Not an html source, sorry!");
		}

		BufferedReader in = new BufferedReader(
				new InputStreamReader(yc.getInputStream(), "UTF-8"));
		String inputLine;
		StringBuilder a = new StringBuilder();
		while ((inputLine = in.readLine()) != null)
			a.append(inputLine);
		in.close();

		return a.toString();
	}

	private String getBodyTextFromHtml(String htmlSource)
	{
		return Jsoup.parseBodyFragment(htmlSource).text();
	}

	private void primaryProcess()
	{
		String robotsTxt = getRobotsTxt();
		if (robotsTxt != null)
		{
			String message = StringUtils.concat(" robots.txt was found");
			socketMessanger.sendMessage(new SocketMessage(message,
					SocketMessageConfiguration.Colors.DARK_SEA_GREEN,
					SocketMessageConfiguration.Icons.EXCLAMATION,
					SocketMessageConfiguration.Colors.DARK_SEA_GREEN));
			website.setRobotsTxt(robotsTxt);
		} else
		{
			String message = StringUtils.concat(" No robots.txt was found");
			socketMessanger.sendMessage(new SocketMessage(message,
					SocketMessageConfiguration.Colors.RED,
					SocketMessageConfiguration.Icons.EXCLAMATION,
					SocketMessageConfiguration.Colors.RED));
		}
		String siteMap = getSiteMap();
		if (siteMap != null)
		{
			String message = StringUtils.concat(" Sitemap.xml was found");
			socketMessanger.sendMessage(new SocketMessage(message,
					SocketMessageConfiguration.Colors.DARK_SEA_GREEN,
					SocketMessageConfiguration.Icons.EXCLAMATION,
					SocketMessageConfiguration.Colors.DARK_SEA_GREEN));
			website.setRobotsTxt(siteMap);
		} else
		{
			String message = StringUtils.concat(" Sitemap.xml was not found");
			socketMessanger.sendMessage(new SocketMessage(message,
					SocketMessageConfiguration.Colors.RED,
					SocketMessageConfiguration.Icons.EXCLAMATION,
					SocketMessageConfiguration.Colors.RED));
		}
		
		String rss = getSiteMap();
		if (rss != null)
		{
			String message = StringUtils.concat(" rss is not supported");
			socketMessanger.sendMessage(new SocketMessage(message,
					SocketMessageConfiguration.Colors.DARK_SEA_GREEN,
					SocketMessageConfiguration.Icons.EXCLAMATION,
					SocketMessageConfiguration.Colors.DARK_SEA_GREEN));
			website.setRobotsTxt(rss);
		} else
		{
			String message = StringUtils.concat(" rss is supported... will try to retreive");
			socketMessanger.sendMessage(new SocketMessage(message,
					SocketMessageConfiguration.Colors.RED,
					SocketMessageConfiguration.Icons.EXCLAMATION,
					SocketMessageConfiguration.Colors.RED));
		}
	}

	/*private void primaryProcessUpdates()
	{
		
	}*/
	public String getRobotsTxt()
	{
		return getRobotsTxt(website.getUrl());
	}

	public String getRobotsTxt(String someDomain)
	{
		try
		{
			return getUrlResult(
					someDomain + "/" + Constants.WebRobotConstants.ROBOTSTXT);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;

	}

	public String getSiteMap()
	{
		return getSiteMap(website.getUrl());
	}

	public String getSiteMap(String someDomain)
	{
		try
		{
			return getUrlResult(
					someDomain + "/" + Constants.WebRobotConstants.SITEMAPXML);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public String getRss()
	{
		return getSiteMap(website.getUrl());
	}

	public String getRss(String someDomain)
	{
		try
		{
			return getUrlResult(someDomain + Constants.WebRobotConstants.RSS);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private void threadSleep(long millis)
	{
		try
		{
			Thread.sleep(millis);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	private String getUrlResult(String urlString) throws IOException
	{
		BufferedReader reader = null;
		try
		{
			URL url = new URL(urlString);
			reader = new BufferedReader(
					new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally
		{
			if (reader != null)
				reader.close();
		}
	}
	
	private List<String> copyList(List<String> inputList)
	{
		List<String> returnList = new ArrayList<String>();
		for (String string : inputList)
		{
			returnList.add(string);
		}
		return returnList;
	}
}
